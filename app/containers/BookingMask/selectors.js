import { createSelector } from 'reselect';
import { initialState } from './ducks';

const selectBookingMask = state => state.bookingmask || initialState;

// Initial State Selectors
export const getDropdownOpened = createSelector(
  selectBookingMask,
  data => data.dropdownOpened,
);

export const getReturnFlight = createSelector(
  selectBookingMask,
  data => data.returnFlight,
);

export const getLocations = createSelector(
  selectBookingMask,
  data => data.locations,
);

// BookingData Selectors
export const selectBookingData = createSelector(
  selectBookingMask,
  state => state.bookingData,
);

export const getOrigin = createSelector(
  selectBookingData,
  data => data.origin,
);

export const getDestination = createSelector(
  selectBookingData,
  data => data.destination,
);

export const getOutboundDate = createSelector(
  selectBookingData,
  data => data.outboundDate,
);

export const getReturnDate = createSelector(
  selectBookingData,
  data => data.returnDate,
);

export const getPassengers = createSelector(
  selectBookingData,
  data => data.passengers,
);

export const getAdultsCount = createSelector(
  getPassengers,
  data => data.adults,
);

export const getChildrenCount = createSelector(
  getPassengers,
  data => data.children,
);

export const getInfantsCount = createSelector(
  getPassengers,
  data => data.infants,
);
