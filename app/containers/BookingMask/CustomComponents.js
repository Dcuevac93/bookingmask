import styled from 'styled-components';
import Background from 'images/booking_mask_bg.jpg';
import { AutoComplete, Menu, Button } from 'antd';

export const MainSection = styled.div`
  width: 100%;
  padding: 3rem 8rem;
  min-height: 75vh;
  margin: auto;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${Background});
`;

export const BookingSection = styled.section`
  padding: 0.5rem 0;
`;

export const OriginSelector = styled(AutoComplete)`
  margin-left: 0;
`;

export const DestinationSelector = styled(AutoComplete)`
  margin-left: 0;
`;

export const BookingDropdownButton = styled.div`
  background-color: white;
  border: 1px solid #ccc;
  height: 100%;
`;

export const BookingMenu = styled(Menu)`
  background: white;
  padding: 0;
`;

export const BookingMenuItem = styled(Menu.Item)`
  background: white;
  padding: 10px;
  border: 1px solid #ccc;
  margin-top: -1px;
  height: 100%;
  cursor: auto;
  &:hover {
    background-color: inherit;
  }
`;

export const BookingMenuItemLabel = styled.label`
  font-size: 1.25rem;
  line-height: 1.2;
  float: left;
  width: 100%;
  margin-right: -110px;

  small {
    font-size: 0.875rem;
    display: block;
    opacity: 0.6;
  }
`;

export const BookingMenuLastItem = styled(BookingMenuItem)`
  text-align: center;
  padding: 1rem;
  &:hover {
    color: #c00;
  }
`;

export const PassangerCountTotal = styled.div`
  font-size: 18px;
  padding: 23px 12px 6px;
`;

export const PassangerCountLabel = styled.div`
  position: absolute;
  pointer-events: none;
  z-index: 2;
  color: #666;
  font-size: 12px;
  left: 12px;
  top: 8px;
  opacity: 0.9;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: calc(100% - 20px);
`;

export const SubmitButton = styled(Button)`
  display: inline-block;
  font-size: 1.25rem;
  font-weight: 300;
  line-height: 1.2;
  vertical-align: middle;
  padding: 12px 20px;
  border: 0;
  text-align: center;
  text-decoration: none;
  cursor: pointer;
  text-shadow: none;
  box-sizing: border-box;
  border-radius: 0;
  background-color: #c00;
  color: white;
  width: 100%;
  height: 56px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  &:hover,
  &:focus {
    color: #fff;
    background-color: #ad0000;
  }
`;

export const LinksList = styled.ul`
  margin-bottom: 0;
  padding-left: 0;
`;

export const LinksListItem = styled.li`
  display: inline-block;
  margin: 0 25px 5px 0;
  list-style: none;

  a {
    display: inline-block;
    text-decoration: none;
    color: #666;
    font-weight: 300;

    &:hover,
    &:focus {
      color: #c00;
    }

    :after {
      content: '';
      margin-left: 10px;
      display: inline-block;
      vertical-align: middle;
      width: 0;
      height: 0;
      border: 4px solid transparent;
      border-left-color: #c00;
      border-right: none;
    }
  }
`;
