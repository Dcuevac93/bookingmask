import * as selectors from '../selectors';

describe('Booking Mask Selectors Tests', () => {
  // The initial state of the App
  const initialState = {
    loading: false,
    error: false,
    dropdownOpened: false,
    returnFlight: true,
    locations: [],
    bookingData: {
      origin: null,
      destination: null,
      outboundDate: null,
      returnDate: null,
      passengers: {
        adults: 0,
        children: 0,
        infants: 0,
        total() {
          return this.adults + this.children + this.infants;
        },
      },
    },
  };

  Object.keys(selectors).forEach(key => {
    it(`${key}`, () => {
      expect(selectors[key](initialState)).toMatchSnapshot();
    });
  });
});
