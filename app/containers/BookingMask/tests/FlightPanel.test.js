import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router-dom';

import FlightPanel from '../Tabs/FlightPanel';
import configureStore from '../../../configureStore';

describe('<FlightPanel /> Component', () => {
  let store;

  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  it('should render and match the snapshot', () => {
    const renderedComponent = renderer
      .create(
        <Provider store={store}>
          <FlightPanel />
        </Provider>,
      )
      .toJSON();

    expect(renderedComponent).toMatchSnapshot();
  });
});
