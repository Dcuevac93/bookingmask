import reducer, {
  initialState,
  openDropdown,
  toogleReturnFlight,
  updateOrigin,
  updateDestination,
  updateOutboundDate,
  updateReturnDate,
  updatePassengers,
} from '../ducks';

describe('Booking Mask Reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toMatchSnapshot();
  });

  it('handles the openDropdown action', () => {
    expect(reducer(initialState, openDropdown(true))).toMatchSnapshot();
  });

  it('handles the toogleReturnFlight action', () => {
    expect(reducer(initialState, toogleReturnFlight(false))).toMatchSnapshot();
  });

  it('handles the updateOrigin action', () => {
    expect(reducer(initialState, updateOrigin('VEN'))).toMatchSnapshot();
  });

  it('handles the updateDestination action', () => {
    expect(reducer(initialState, updateDestination('MIA'))).toMatchSnapshot();
  });

  it('handles the updateOutboundDate action', () => {
    expect(
      reducer(initialState, updateOutboundDate('0000-00-00')),
    ).toMatchSnapshot();
  });

  it('handles the updateReturnDate action', () => {
    expect(
      reducer(initialState, updateReturnDate('0000-00-00')),
    ).toMatchSnapshot();
  });

  it('handles the updatePassengers action (Adults)', () => {
    expect(
      reducer(initialState, updatePassengers('adults', 5)),
    ).toMatchSnapshot();
  });

  it('handles the updatePassengers action (Children)', () => {
    expect(
      reducer(initialState, updatePassengers('children', 5)),
    ).toMatchSnapshot();
  });

  it('handles the updatePassengers action (Infants)', () => {
    expect(
      reducer(initialState, updatePassengers('infants', 5)),
    ).toMatchSnapshot();
  });
});
