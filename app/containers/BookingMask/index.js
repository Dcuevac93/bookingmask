/*
 * BookingMask
 *
 * This is the first thing users see, at the '/' route
 */

// Import function component
import BookingMask from './BookingMask';

export default BookingMask;
