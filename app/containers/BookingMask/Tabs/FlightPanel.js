/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
// Import Selectors
import {
  submitBookingData,
  openDropdown,
  toogleReturnFlight,
  locationsRequest,
  updateOrigin,
  updateDestination,
  updateOutboundDate,
  updateReturnDate,
  updatePassengers,
} from 'containers/BookingMask/ducks';
// Import Actions
import {
  getDropdownOpened,
  getReturnFlight,
  getLocations,
  getOrigin,
  getDestination,
  getOutboundDate,
  getReturnDate,
  getPassengers,
} from 'containers/BookingMask/selectors';
// UI / UX
import {
  Row,
  Col,
  DatePicker,
  Dropdown,
  InputNumber,
  Switch,
  Form,
} from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLongArrowAltRight as faArrowRight,
  faExchangeAlt,
} from '@fortawesome/free-solid-svg-icons';
import {
  BookingSection,
  OriginSelector,
  DestinationSelector,
  PassangerCountTotal,
  PassangerCountLabel,
  BookingDropdownButton,
  BookingMenu,
  BookingMenuItem,
  BookingMenuItemLabel,
  BookingMenuLastItem,
  SubmitButton,
  LinksList,
  LinksListItem,
} from 'containers/BookingMask/CustomComponents';
import moment from 'moment';

/**
 * Functional Component
 */
const FlightPanel = ({
  // Selectors
  dropdownOpened,
  returnFlight,
  locations,
  passengers,
  returnDate,
  // Actions
  onSubmitBookingData,
  onOpenDropdown,
  onToogleReturnFlight,
  onLocationsRequest,
  onUpdateOrigin,
  onUpdateDestination,
  onUpdateOutboundDate,
  onUpdateReturnDate,
  onUpdatePassengers,
  // Form
  form,
}) => {
  const { getFieldDecorator } = form;

  // Can not select days before today and today
  const disabledDate = current => current && current < moment().endOf('day');

  const handleVisibleChange = flag => {
    onOpenDropdown(flag);
  };

  const handleSubmit = e => {
    e.preventDefault();
    form.validateFieldsAndScroll(err => {
      if (!err) {
        onSubmitBookingData();
      }
    });
  };

  const menu = (
    <BookingMenu>
      <BookingMenuItem key="0">
        <Row>
          <Col xs={24} md={12}>
            <BookingMenuItemLabel>
              Adults
              <small>+12 years</small>
            </BookingMenuItemLabel>
          </Col>
          <Col xs={24} md={12}>
            <InputNumber
              min={0}
              max={60}
              defaultValue={0}
              onChange={val => onUpdatePassengers('adults', val)}
            />
          </Col>
        </Row>
      </BookingMenuItem>
      <BookingMenuItem key="1">
        <Row>
          <Col xs={24} md={12}>
            <BookingMenuItemLabel>
              Children
              <small>2-11 years</small>
            </BookingMenuItemLabel>
          </Col>
          <Col xs={24} md={12}>
            <InputNumber
              min={0}
              max={60}
              defaultValue={0}
              onChange={val => onUpdatePassengers('children', val)}
            />
          </Col>
        </Row>
      </BookingMenuItem>
      <BookingMenuItem key="3">
        <Row>
          <Col xs={24} md={12}>
            <BookingMenuItemLabel>
              Infants
              <small>0-1 years</small>
            </BookingMenuItemLabel>
          </Col>
          <Col xs={24} md={12}>
            <InputNumber
              min={0}
              max={60}
              defaultValue={0}
              onChange={val => onUpdatePassengers('infants', val)}
            />
          </Col>
        </Row>
      </BookingMenuItem>
      <BookingMenuLastItem key="4" onClick={() => onOpenDropdown(false)}>
        Close
      </BookingMenuLastItem>
    </BookingMenu>
  );

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <BookingSection>
          <Row>
            <Col xs={12} md={6} lg={4}>
              <Form.Item>
                {getFieldDecorator('origin', {
                  rules: [
                    {
                      required: true,
                      message: 'Please select an origin',
                    },
                  ],
                })(
                  <OriginSelector
                    placeholder="From"
                    onChange={value => onUpdateOrigin(value)}
                    onSearch={search => onLocationsRequest({ search })}
                    dataSource={locations.map(location => ({
                      value: location.alpha3Code,
                      text: `${location.name} (${location.alpha3Code})`,
                    }))}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col xs={12} md={6} lg={4}>
              <Form.Item>
                {getFieldDecorator('destination', {
                  rules: [
                    {
                      required: true,
                      message: 'Please select a destination',
                    },
                  ],
                })(
                  <DestinationSelector
                    placeholder="To"
                    onChange={value => onUpdateDestination(value)}
                    onSearch={search => onLocationsRequest({ search })}
                    dataSource={locations.map(location => ({
                      value: location.alpha3Code,
                      text: `${location.name} (${location.alpha3Code})`,
                    }))}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col xs={24} md={4} lg={4}>
              <Form.Item>
                {getFieldDecorator('outboundFlight', {
                  rules: [
                    {
                      required: true,
                      message: 'Please select an outbound date',
                    },
                  ],
                })(
                  <DatePicker
                    placeholder="Outbound Flight"
                    onChange={(value, dateString) => {
                      onUpdateOutboundDate(dateString);
                    }}
                    disabledDate={disabledDate}
                    format="YYYY-MM-DD"
                  />,
                )}
              </Form.Item>
            </Col>
            <Col xs={24} md={4} lg={2} style={{ textAlign: 'center' }}>
              <Switch
                checkedChildren={<FontAwesomeIcon icon={faExchangeAlt} />}
                unCheckedChildren={<FontAwesomeIcon icon={faArrowRight} />}
                onChange={onToogleReturnFlight}
                defaultChecked={returnFlight}
              />
            </Col>
            <Col xs={24} md={4} lg={4}>
              <Form.Item>
                {getFieldDecorator('returnFlight', {
                  initialValue: returnDate ? moment(returnDate) : undefined,
                  rules: [
                    {
                      required: returnFlight,
                      message: 'Please select an return date',
                    },
                  ],
                })(
                  <DatePicker
                    placeholder="Return Flight"
                    disabled={!returnFlight}
                    disabledDate={disabledDate}
                    format="YYYY-MM-DD"
                    onChange={(value, dateString) => {
                      onUpdateReturnDate(dateString);
                    }}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col xs={24} lg={6}>
              <Dropdown
                overlay={menu}
                trigger={['click']}
                onVisibleChange={handleVisibleChange}
                visible={dropdownOpened}
              >
                <BookingDropdownButton>
                  <PassangerCountLabel>
                    {`${passengers.adults} Adults,
                    ${passengers.children} Children,
                    ${passengers.infants} Infants`}
                  </PassangerCountLabel>
                  <PassangerCountTotal>
                    {`${passengers.total()} Passengers`}
                  </PassangerCountTotal>
                </BookingDropdownButton>
              </Dropdown>
            </Col>
          </Row>
        </BookingSection>
        <BookingSection>
          <Row type="flex" justify="space-around" align="middle">
            <Col md={24} lg={18}>
              <LinksList>
                <LinksListItem>
                  <a href="#">Advanced Search</a>
                </LinksListItem>
                <LinksListItem>
                  <a href="#">Arrivals and departures</a>
                </LinksListItem>
                <LinksListItem>
                  <a href="#">Inspire me</a>
                </LinksListItem>
                <LinksListItem>
                  <a href="#">Miles & more</a>
                </LinksListItem>
                <LinksListItem>
                  <a href="#">Check-in</a>
                </LinksListItem>
              </LinksList>
            </Col>
            <Col md={24} lg={6}>
              <SubmitButton htmlType="submit">Search</SubmitButton>
            </Col>
          </Row>
        </BookingSection>
      </Form>
    </>
  );
};

FlightPanel.propTypes = {
  // Selectors
  dropdownOpened: PropTypes.bool,
  returnFlight: PropTypes.bool,
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  passengers: PropTypes.shape({}),
  returnDate: PropTypes.string,
  // Actions
  onSubmitBookingData: PropTypes.func,
  onOpenDropdown: PropTypes.func,
  onToogleReturnFlight: PropTypes.func,
  onLocationsRequest: PropTypes.func,
  onUpdateOrigin: PropTypes.func,
  onUpdateDestination: PropTypes.func,
  onUpdateOutboundDate: PropTypes.func,
  onUpdateReturnDate: PropTypes.func,
  onUpdatePassengers: PropTypes.func,
  // Form
  form: PropTypes.shape({}),
};

// Map selectors
const mapStateToProps = state => ({
  dropdownOpened: getDropdownOpened(state),
  returnFlight: getReturnFlight(state),
  locations: getLocations(state),
  origin: getOrigin(state),
  destination: getDestination(state),
  outboundDate: getOutboundDate(state),
  returnDate: getReturnDate(state),
  passengers: getPassengers(state),
});

// Map actions
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onSubmitBookingData: submitBookingData,
      onOpenDropdown: openDropdown,
      onToogleReturnFlight: toogleReturnFlight,
      onLocationsRequest: locationsRequest,
      onUpdateOrigin: updateOrigin,
      onUpdateDestination: updateDestination,
      onUpdateOutboundDate: updateOutboundDate,
      onUpdateReturnDate: updateReturnDate,
      onUpdatePassengers: updatePassengers,
    },
    dispatch,
  );

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const WrappedFlightPanel = Form.create()(FlightPanel);

export default compose(
  withConnect,
  memo,
)(WrappedFlightPanel);
