import React from 'react';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
// UI / UX
import {
  Tab,
  Tabs,
  TabList,
  TabPanel,
  TabLink,
  TabText,
} from 'components/BookingTabs';
import { Row, Col } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPlane,
  faPlaneDeparture,
  faBed,
  faCar,
  faCrown,
} from '@fortawesome/free-solid-svg-icons';

import { MainSection } from './CustomComponents';
import FlightPanel from './Tabs/FlightPanel';

import reducer from './ducks';
import saga from './saga';

const key = 'bookingmask';

/**
 * Functional Component
 */
const BookingMask = () => {
  // Inject Reducer & Saga
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  return (
    <MainSection>
      <Row>
        <Col xs={24}>
          <Tabs>
            <TabList>
              <Tab>
                <TabLink href="#">
                  <FontAwesomeIcon icon={faPlane} size="2x" />
                  <TabText>Flight</TabText>
                </TabLink>
              </Tab>
              <Tab>
                <TabLink href="#">
                  <FontAwesomeIcon icon={faPlaneDeparture} size="2x" />
                  <TabText>Stopover</TabText>
                </TabLink>
              </Tab>
              <Tab>
                <TabLink href="#">
                  <FontAwesomeIcon icon={faBed} size="2x" />
                  <TabText>Hotel</TabText>
                </TabLink>
              </Tab>
              <Tab>
                <TabLink href="#">
                  <FontAwesomeIcon icon={faCar} size="2x" />
                  <TabText>Rental Car</TabText>
                </TabLink>
              </Tab>
              <Tab>
                <TabLink href="#">
                  <FontAwesomeIcon icon={faCrown} size="2x" />
                  <TabText>SWISS Choice</TabText>
                </TabLink>
              </Tab>
            </TabList>
            <TabPanel>
              <FlightPanel />
            </TabPanel>
            <TabPanel>Any content 2</TabPanel>
            <TabPanel>Any content 3</TabPanel>
            <TabPanel>Any content 4</TabPanel>
            <TabPanel>Any content 5</TabPanel>
          </Tabs>
        </Col>
      </Row>
    </MainSection>
  );
};

export default BookingMask;
