/**
 * Handles Booking Process
 */
import {
  call,
  all,
  put,
  select,
  takeLatest,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';

// Import actions
import {
  submitBookingData,
  locationsRequest,
  locationsSuccess,
  locationsFailure,
} from './ducks';

// Import Selectors
import {
  getOrigin,
  getDestination,
  getOutboundDate,
  // getReturnDate,
  getReturnFlight,
  getAdultsCount,
  getChildrenCount,
  getInfantsCount,
} from './selectors';

/**
 * Countries request/response handler
 */
export function* getLocations(action) {
  const { search } = action.payload;
  const requestURL = `https://restcountries.eu/rest/v2/name/${search}`;
  try {
    // Call request helper (see 'utils/request')
    const locations = yield call(request, requestURL);
    yield put(locationsSuccess(locations));
  } catch (err) {
    yield put(locationsFailure(err));
  }
}

/**
 * Build and open URL based on Booking data
 */
export function* sendBookingData() {
  // Select Booking Data from store
  const origin = yield select(getOrigin);
  const destination = yield select(getDestination);
  const outboundDate = yield select(getOutboundDate);
  const returnFlight = yield select(getReturnFlight);
  // const returnDate = yield select(getReturnDate);
  const adults = yield select(getAdultsCount);
  const children = yield select(getChildrenCount);
  const infants = yield select(getInfantsCount);

  const flightType = returnFlight ? 'Return' : 'Outbound';

  // Build URL
  const bookingURL = `https://www.swiss.com/us/en/Book/${flightType}/${origin}-${destination}/from-${outboundDate}/adults-${adults}/children-${children}/infants-${infants}/class-economy/al-LX/sidmbvl`;

  yield window.open(bookingURL);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* containerSaga() {
  yield all([
    debounce(500, locationsRequest().type, getLocations),
    takeLatest(submitBookingData().type, sendBookingData),
  ]);
}
