/**
 *
 * globalRoutes.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Skeleton } from 'antd';
import BookingMask from 'containers/BookingMask/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

export default function Routes() {
  return (
    <Suspense maxDuration={300} fallback={<Skeleton active />}>
      <Switch>
        <Route exact path="/" render={() => <BookingMask />} />
        <Route render={() => <NotFoundPage />} />
      </Switch>
    </Suspense>
  );
}
