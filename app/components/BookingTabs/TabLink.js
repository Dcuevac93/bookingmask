import styled from 'styled-components';

const TabLink = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  text-align: center;
  overflow: hidden;
  min-width: 150px;
  height: 100%;
  padding: 12px 2px;
`;

export default TabLink;
