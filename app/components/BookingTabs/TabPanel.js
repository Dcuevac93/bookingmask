import styled from 'styled-components';
import { TabPanel } from 'react-tabs';

export const TabPanelStyled = styled(TabPanel)`
  display: none;

  &.react-tabs__tab-panel--selected {
    display: block;
    padding: 1.5rem;
    background: rgba(255, 255, 255, 0.9);
  }
`;

export default TabPanelStyled;
