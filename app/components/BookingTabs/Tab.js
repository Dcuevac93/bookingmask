import styled from 'styled-components';

import { Tab } from 'react-tabs';

export const TabStyled = styled(Tab)`
  display: block;
  width: 100%;
  flex: 0 1 auto; /* Default */
  list-style-type: none;
  margin-right: 2px;
  border: 1px solid transparent;
  border-bottom: none;
  bottom: -1px;
  position: relative;
  padding: 6px 12px;
  cursor: pointer;
  background-color: rgba(51, 51, 51, 0.8);

  a {
    color: #fff;
  }

  &.react-tabs__tab--selected {
    text-decoration: none;
    text-align: center;
    background: rgba(255, 255, 255, 0.9);

    a {
      color: #c00;
    }
  }

  &.react-tabs__tab--disabled {
    color: GrayText;
    cursor: default;
  }

  &:focus {
    box-shadow: 0 0 5px hsl(208, 99%, 50%);
    border-color: hsl(208, 99%, 50%);
    outline: none;
  }
`;

export default TabStyled;
