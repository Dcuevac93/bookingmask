import styled from 'styled-components';

const TabText = styled.div`
  margin-left: 1rem;
  display: inline-block;
`;

export default TabText;
