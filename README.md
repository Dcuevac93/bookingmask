# Booking Mask

Booking mask exercise based on https://www.swiss.com/us/en

## Getting Started

Clone the Repo
```
git clone https://Dcuevac93@bitbucket.org/Dcuevac93/bookingmask.git
```
Install dependencies
```
npm install
```
Run the project
```
npm start
```

## Running the tests
```
npm run test
```

## Author

**David Cueva**
